#
# Cookbook Name:: miner-cookbook
# Recipe:: default
#
# Copyright (C) 2013 YOUR_NAME
# 
# All rights reserved - Do Not Redistribute
#

username = node[:miner][:user]
group = node[:miner][:group]

home_dir = "/home/#{node[:miner][:user]}"

group node[:miner][:user]

user node[:miner][:user] do
	supports :manage_home => true
	group node[:miner][:group]
	system true
	shell "/bin/bash"
	home home_dir
end

include_recipe "apt"
include_recipe "build-essential"

# install bfgminer

apt_repository "unit3_bfgminer" do
  uri "http://ppa.launchpad.net/unit3/bfgminer/ubuntu"
  distribution node['lsb']['codename']
  components ["main"]
  keyserver "keyserver.ubuntu.com"
  key "0AE7FCDA"  
end

package "bfgminer" do
	action :install
end

# install cuda

remote_file "#{Chef::Config[:file_cache_path]}/cuda-repo.deb" do
  source "http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1204/x86_64/cuda-repo-ubuntu1204_5.5-0_amd64.deb"
end

dpkg_package "cuda-repo" do
	source "#{Chef::Config[:file_cache_path]}/cuda-repo.deb"
	action :install
	notifies :run, resources(:execute => "apt-get update"), :immediately
end

package "cuda" do
	action :install
end
